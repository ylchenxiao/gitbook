<?php

namespace app\controllers;

use Yii;
use app\models\IssueLog;
use app\models\IssueLogSearch;
use app\controllers\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\widgets\Parsedown;

/**
 * IssueLogController implements the CRUD actions for IssueLog model.
 */
class IssueLogController extends BaseController
{

    protected $mustlogin = ['index', 'view', 'create', 'update', 'delete', 'export', 'table', 'upload'];

    public function actions()
    {
        return [
            'upload' => [
                'class' => 'kucha\ueditor\UEditorAction',
                'config' => [
                    "imageUrlPrefix"  => "http://10.7.40.13:10001",//图片访问路径前缀
                    "imagePathFormat" => "/upload/image/{yyyy}{mm}{dd}/{time}{rand:6}", //上传保存路径
                    "catcherMaxSize"  => 20480000,
                    "fileMaxSize"     => 20480000,
                    "imageMaxSize"    => 20480000,

                ],
            ]
        ];
    }
    /**
     * Lists all IssueLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IssueLogSearch();

        $data = Yii::$app->request->queryParams;
        if (empty($data)) {
            $searchModel['week'] = $data['week'] = date('W');
            $searchModel['author'] = $data['author'] = Yii::$app->user->identity->username;
        }
        $dataProvider = $searchModel->search($data);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IssueLog model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IssueLog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new IssueLog();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing IssueLog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IssueLog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IssueLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IssueLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IssueLog::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionExport($q = '')
    {
        $queryParam = (array)json_decode(base64_decode($q), true);

        $searchModel = new IssueLogSearch();
        $data = $searchModel->search($queryParam)->getModels();
        $md = $this->createMarkDown($data);


        $response = Yii::$app->getResponse();
        $response->format = $response::FORMAT_RAW;
        $response->getHeaders()->set('Content-Type', 'Content-type: application/octet-stream');
        $response->getHeaders()->set('Content-Disposition', 'attachment;filename="周报.md');
        $response->getHeaders()->set('Cache-Control', 'max-age=0');
        return $md;
    }

    private function createMarkDown($models)
    {
        $export = [];
        foreach ($models as $_info) {
            $export[$_info['author']][$_info['week']][$_info['cate']][] = $_info->attributes;
        }
        $md = '';
        foreach ($export as $_a => $weeks) {
            $md .= "# {$_a} 周报 #\n\n";
            foreach ($weeks as $_w => $week) {
                $md .= "## 第 {$_w} 周 ##\n\n";
                foreach ($week as $_c => $cates) {
                    $md .= " * {$_c}\n";
                    foreach ($cates as $info) {
                        $md .= "     * [{$info['title']}](http://10.7.40.13:10001/issue-log/view?id={$info['id']})\n";
                        $_str = explode(PHP_EOL, $info['reason']);
                        foreach ($_str as $_i => $_r) {
                            $md .= "         * {$_r}\n";
                        }
                    }
                }
                $md .= "\n";
            }
            $md .= "\n";
        }
        return $md;
    }

    public function actionTable($q = '')
    {
        $queryParam = (array)json_decode(base64_decode($q), true);

        $searchModel = new IssueLogSearch();
        $data = $searchModel->search($queryParam)->getModels();
        $html = $this->createTable($data);
        return $html;
    }

    protected function createTable($models)
    {
        $md = $this->createMarkDown($models);
        $parse = new Parsedown;
        return $parse->text($md);
    }
}
