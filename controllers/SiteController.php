<?php

namespace app\controllers;

use app\controllers\BaseController;
use app\models\ContactForm;
use app\models\UAuthForm;
use app\models\Homepage;
use Yii;
use yii\web\Response;

class SiteController extends BaseController {

	protected $except = ['index','login'];
	protected $mustlogin = [ 'logout'];

	/**
	 * 首页
	 *
	 * @return string
	 */
	public function actionIndex() {

		return $this->render('index', [
			'notice' => Homepage::find()->where(['type'=>1])->orderBy('sort DESC')->asArray()->one(),
			'hots' => Homepage::find()->where(['type'=>2])->orderBy('sort DESC')->asArray()->all(),
		]);
	}

	/**
	 * 登录
	 *
	 * @return Response|string
	 */
	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			var_dump(Yii::$app->user);die;
			$this->goHome();
		}
		$model = new UAuthForm();
		$model->setAttributes(Yii::$app->request->get());
		if ($model->login()) {
			return $this->goHome();
		}
	}

	/**
	 * 登出
	 *
	 * @return Response
	 */
	public function actionLogout() {
		Yii::$app->user->logout();
		$model = new UAuthForm();
		$model->logout();
	}
}
