<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\behaviors\LogBehavior;

class BaseController extends Controller {

    // 默认必须登录可才能访问所有管理页面
    protected $mustlogin = ['index', 'view', 'create', 'update', 'delete'];
    protected $actions = ['*'];
    protected $except = [];
    protected $verbs = [];


    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => $this->actions,
                'except' => $this->except,
                'rules' => [
                    [
                        'allow' => false,
                        'actions' => empty($this->mustlogin) ? [] : $this->mustlogin,
                        'roles' => ['?'], // 游客
                    ],
                    [
                        'allow' => true,
                        'actions' => empty($this->mustlogin) ? [] : $this->mustlogin,
                        'roles' => ['@'], // 认证用户
                    ],
                ],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => $this->verbs,
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ]
        ];
    }

    public function missAction() {
        var_dump($this);die;
    }
}
