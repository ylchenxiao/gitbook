<?php


namespace app\assets;

use yii\web\AssetBundle;


class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'static/css/bootstrap.min.css',
        'static/css/font-awesome.min.css',
        // 'tatic/css/weather-icons.min.css',
        'static/css/f1cc135d43484d09b549095f725b2b04.css',
        'static/css/beyond.min.css',
        'static/css/color-json.css',
        'static/css/code.css',
        // 'static/css/typicons.min.css',
        // 'static/css/animate.min.css',
    ];
    public $js = [
        ['static/js/skins.min.js', 'position' => \yii\web\View::POS_HEAD],
        ['static/layer/layer.js', 'position' => \yii\web\View::POS_END],
        // ['static/js/jquery-2.0.3.min.js', 'position' => \yii\web\View::POS_END],
        // ['static/js/bootstrap.min.js', 'position' => \yii\web\View::POS_END],
        ['static/js/beyond.min.js', 'position' => \yii\web\View::POS_END],
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
