<?php

$params = require __DIR__ . '/' . YII_ENV . '/params.php';
$db = require __DIR__ . '/' . YII_ENV . '/db.php';
$log = require __DIR__ . '/' . YII_ENV . '/log.php';

$config = [
    'id' => 'gitbook',
    'name' => 'GitBook',
    'language' => 'zh-CN',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'DncMHDF2xGxjTC_ScWQmYNYqJD8ANhrf',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\UAuth',
            'loginUrl' => ['site/login'],
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => $log,
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
            ],
        ],
    ],
    'params' => $params,
    'as Log' => 'app\behaviors\LogBehavior',
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*', '::1'],
        'generators' => [
            'crud' => [ //生成器名称
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [ //设置我们自己的模板
                    //模板名 => 模板路径
                    '亮色主题-white1' => '@app/gii/generators/crud/default',
                ],
            ],
            'model' => [ //生成器名称
                'class' => 'yii\gii\generators\model\Generator',
                'templates' => [ //设置我们自己的模板
                    //模板名 => 模板路径
                    '亮色主题-white1' => '@app/gii/generators/model/default',
                ],
            ],
        ],
    ];
}

return $config;
