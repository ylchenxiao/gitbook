<?php

return [
	'traceLevel' => YII_DEBUG ? 3 : 0,
	'targets' => [
		[
			'class' => 'yii\log\FileTarget',
			'levels' => ['error', 'warning'],
	        'logVars'=>['_GET','_POST'],
	        'categories'=>['application'],
	        'logFile'=>'@runtime/logs/LOG'.date('y_m_d').'.log',
		],
	    [
	        'class' => 'yii\log\FileTarget',
	        'levels' => ['error', 'warning', 'info'],
	        'logVars'=>[],
	        //表示以yii\db\或者app\models\开头的分类都会写入这个文件
	        'categories'=>['app\models\*'],
	        //表示写入到文件
	        'logFile'=>'@runtime/logs/SQL_'.date('y_m_d').'.log',
	        'maxFileSize' => 1024 * 200,  //设置文件大小，以k为单位
	        'fileMode' => 0775, //设置日志文件权限
	        'maxLogFiles' => 20,  //同个文件名最大数量
	        'rotateByCopy' => false, //是否以复制的方式rotate
	        'prefix' => '',   //日志格式自定义 回调方法
	    ],
	],
];