<?php

return [
	'class' => 'yii\db\Connection',
	'dsn' => 'mysql:host=127.0.0.1;dbname=git_book',
	'username' => 'root',
	'password' => '123456',
	'charset' => 'utf8',
	'enableSchemaCache' => true,
	'schemaCacheDuration' => 60,
	'schemaCache' => 'cache',
];
