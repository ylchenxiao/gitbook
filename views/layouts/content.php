<div class="content-wrapper">
    <section class="content" id="in_content">
        <?=$content?>
    </section>
    <section class="content" style="padding: 0px;display: none;height: 100%;" id="iframe_content">
		<iframe
            src=""
            id="iframe_target"
            style="width:100%;border:none;height: 767px;"
            sandbox="allow-scripts allow-same-origin allow-popups allow-forms"
        >
        </iframe>
    </section>
</div>
