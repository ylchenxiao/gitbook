<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Permission;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="main-container container-fluid">
    <div class="page-container">
        <!-- 起：侧边栏 -->
        <div class="page-sidebar" id="sidebar">
            <?= $this->render('sidebar', ['menu'=>Permission::tree(Yii::$app->user->isGuest ? '' : Yii::$app->user->id)]);?>
        </div>
        <!-- 终：侧边栏 -->

        <!-- 始：内容页 -->
        <div class="page-content">
            <!-- 始：面包屑 -->
            <div class="page-breadcrumbs">
                <?= $this->render('breadcrumb');?>
            </div>
            <!-- 终：面包屑 -->

            <!-- 始：标题栏 -->
            <div class="page-header position-relative">
                <?= $this->render('page-header');?>
            </div>
            <!-- 终：标题栏 -->

            <!-- 始：详情页面 -->
            <div class="page-body">
                <div class="widget-body">
                    <?= $this->render('content', ['content'=>$content])?>
                </div>
            </div>
            <!-- 终：页面详情 -->
        </div>
        <!-- 终：内容页 -->
    </div>
</div>


<?php
$js = <<<'JS'
$(function(){
    // $('#iframe_target').attr('height',$('div.content-wrapper').height() - $('section.content-header').outerHeight(true) -5);

    function refreshIframe(objA){
        console.log(objA);
        $('#iframe_target').attr('src', objA.href);
        $('.content-header>h1').text($(objA).find('span').text());
        $('#iframe_content').show();
        $('#in_content').hide();
    }

    $('ul.sidebar-menu li>a').click(function(ev){
        if($(this).attr('flag') == 2){
            ev.preventDefault();
            refreshIframe(this);
            return false;
        }
        return true;
    });
});
JS;
$this->registerJs($js);?>
</body>
<?php $this->endBody()?>
</html>
<?php $this->endPage()?>