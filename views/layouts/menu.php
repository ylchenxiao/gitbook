<?php foreach ($menu as $item) : ?>
<li>
    <?php if (!empty($item['son'])) : ?>
    <!-- 有子菜单 -->
    <a href="#" id="menu_<?=$item['id']?>" class="menu-dropdown">
        <i class="menu-icon <?= $item['icon']?>" aria-hidden="true"></i>
        <span class="menu-text"> <?=Yii::t('menu', $item['label'])?> </span>
        <i class="menu-expand"></i>
    </a>
    <ul class="submenu">
        <?= $this->render('menu', ['menu'=>$item['son']])?>
    </ul>
    <?php else : ?>
    <!-- 无子菜单 -->
    <a  id="item_<?=$item['id']?>" href="<?= $item['url']?>"  flag="<?= $item['flag'] ?>">
        <i class="menu-icon <?= $item['icon']?>" aria-hidden="true"></i>
        <span class="menu-text"> <?=Yii::t('menu', $item['label'])?> </span>
    </a>
    <?php endif; ?>
</li>
<?php endforeach; ?>