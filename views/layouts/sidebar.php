<?php
use yii\helpers\Html;

?>
<!-- 开始：提示信息栏 -->
<div class="sidebar-header-wrapper user-info">
    <?php if (!Yii::$app->user->isGuest) : ?>
        <img src="<?=Yii::$app->user->identity->avatar;?>" class="img-circle" alt="User Image"/>
        <p><?=Yii::$app->user->identity->username;?></p>
        <?=Html::a('退出', ['/site/logout'], ['data-method' => 'post', 'class' => ''])?>
        <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
    <?php else : ?>
        <p>游客，请</p>
        <?=Html::a('登录', ['/site/login'], ['data-method' => 'post', 'class' => ''])?>
        <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
    <?php endif;?>
</div>
<!-- 结束：提示信息栏 -->
<ul class="nav sidebar-menu">
    <?= $this->render('menu', ['menu'=>$menu])?>
</ul>