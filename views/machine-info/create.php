<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MachineInfo */

$this->title = 'Create Machine Info';
$this->params['breadcrumbs'][] = ['label' => 'Machine Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="machine-info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
