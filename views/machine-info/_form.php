<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\MachineInfo;
use kartik\widgets\Typeahead;

/* @var $this yii\web\View */
/* @var $model app\models\MachineInfo */
/* @var $form yii\widgets\ActiveForm */

$listProject = array_keys(MachineInfo::find()
    ->asArray()
    ->indexBy('project')
    ->all());
$listProject = array_combine($listProject, $listProject);
?>

<div class="machine-info-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'project')->widget(Typeahead::class, [
        'dataset' => [[
            'local' => MachineInfo::uniqueColumn('project'),
            'limit' => 10
        ]],
        'pluginOptions' => ['highlight' => true],
        'options' => ['placeholder' => 'Filter as you type ...'],
    ]); ?>

    <?= $form->field($model, 'machine_room')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'domain')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'machine_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'machine_remarks')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'business_desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'remarks')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'extend')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
