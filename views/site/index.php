<?php

$this->title = '欢迎使用';

?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?=$notice['title']??''?>!</h1>
        <p class="lead"><?=$notice['desc']??''?></p>
        <p><a class="btn btn-lg btn-success" target="_blank" href="<?=$notice['url']??''?>"><?=$notice['url_name']??''?></a></p>
    </div>

    <div class="body-content">
        <div class="row">
            <?php foreach ($hots as $_h):?>
            <div class="col-lg-4">
                <h2><?=$_h['title']??''?></h2>
                <p><?=$_h['desc']??''?></p>
                <p><a class="btn btn-default" target="_blank" href="<?=$_h['url']??''?>"><?=$_h['url_name']??''?></a></p>
            </div>
            <?php endForeach;?>
        </div>
    </div>
</div>
