<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MachineInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Machine Infos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="machine-info-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Machine Info', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'project',
            'machine_room',
            'domain',
            'machine_type',
            //'ip',
            //'machine_remarks',
            //'business_desc',
            //'remarks',
            //'extend',
            //'ctime:datetime',
            //'mtime:datetime',
            //'author',
            //'modifer',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
