<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MachineInfo */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Machine Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="machine-info-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'project',
            'machine_room',
            'domain',
            'machine_type',
            'ip',
            'machine_remarks',
            'business_desc',
            'remarks',
            'extend',
            'ctime:datetime',
            'mtime:datetime',
            'author',
            'modifer',
        ],
    ]) ?>

</div>
