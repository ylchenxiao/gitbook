<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MachineInfoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="machine-info-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'project') ?>

    <?= $form->field($model, 'machine_room') ?>

    <?= $form->field($model, 'domain') ?>

    <?= $form->field($model, 'machine_type') ?>

    <?php // echo $form->field($model, 'ip') ?>

    <?php // echo $form->field($model, 'machine_remarks') ?>

    <?php // echo $form->field($model, 'business_desc') ?>

    <?php // echo $form->field($model, 'remarks') ?>

    <?php // echo $form->field($model, 'extend') ?>

    <?php // echo $form->field($model, 'ctime') ?>

    <?php // echo $form->field($model, 'mtime') ?>

    <?php // echo $form->field($model, 'author') ?>

    <?php // echo $form->field($model, 'modifer') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
