<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IssueLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = (empty($searchModel['week']) ? '': '第'.$searchModel['week'].'周').'问题记录';
$this->params['breadcrumbs'][] = $this->title;
            $queryParam = base64_encode(json_encode(['IssueLogSearch'=>$searchModel->attributes]));
?>
<div class="issue-log-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('创建', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('导出周报', ['export?q='.$queryParam], ['class' => 'btn btn-success']) ?>
        <?= Html::a('查看周报', ['table?q='.$queryParam], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'cate',
            'title',
            'reason:html',
            [
                "attribute" => 'issue',
                'options' => [
                    'width' => 180
                ],
                'format' => 'html'
            ],
            // [
            //     "attribute" => 'fix',
            //     'headerOptions' => ['width' => '10%'],
            //     'format' => 'html'
            // ],
            'week',
            [
                'attribute' => 'author',
                'value'=>function ($model) {
                    return $model->author . "\r\n" . date('Y年m月d日 H:i:s', $model->ctime);
                },
                'headerOptions' => ['width' => '200'],
                'format' =>'ntext'
            ],
            //'extend',
            // 'ctime:datetime',
            //'mtime:datetime',
            //'author',
            //'modifer',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php //Pjax::end(); ?>
</div>
