<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IssueLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="issue-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'cate') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'reason') ?>

    <?= $form->field($model, 'issue') ?>

    <?php // echo $form->field($model, 'fix') ?>

    <?php // echo $form->field($model, 'extend') ?>

    <?php // echo $form->field($model, 'ctime') ?>

    <?php // echo $form->field($model, 'mtime') ?>

    <?php // echo $form->field($model, 'author') ?>

    <?php // echo $form->field($model, 'modifer') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
