<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IssueLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="issue-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cate')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reason')->widget('kucha\ueditor\UEditor', [
        'clientOptions' => [
            'initialFrameHeight' => '200',
            'lang' =>'zh-cn', //中文为 zh-cn
            //定制菜单
            'toolbars' => [
                [
                    'fullscreen', 'source', 'undo', 'redo', '|',
                    'fontsize',
                    'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'removeformat',
                    'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|',
                    'forecolor', 'backcolor', '|',
                    'lineheight', '|', 'indent', '|',
                    'simpleupload', 'imagenone', 'imageleft', 'imageright', 'imagecenter', 'insertimage', '|',

                ],
            ],
        ],
    ]);?>

    <?= $form->field($model, 'issue')->widget('kucha\ueditor\UEditor', [
        'clientOptions' => [
            'initialFrameHeight' => '200',
            'lang' =>'zh-cn', //中文为 zh-cn
            //定制菜单
            'toolbars' => [
                [
                    'fullscreen', 'source', 'undo', 'redo', '|',
                    'fontsize',
                    'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'removeformat',
                    'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|',
                    'forecolor', 'backcolor', '|',
                    'lineheight', '|', 'indent', '|',
                    'simpleupload', 'imagenone', 'imageleft', 'imageright', 'imagecenter', 'insertimage', '|',

                ],
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'fix')->widget('kucha\ueditor\UEditor', [
        'clientOptions' => [
            'initialFrameHeight' => '200',
            'lang' =>'zh-cn', //中文为 zh-cn
            //定制菜单
            'toolbars' => [
                [
                    'fullscreen', 'source', 'undo', 'redo', '|',
                    'fontsize',
                    'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'removeformat',
                    'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|',
                    'forecolor', 'backcolor', '|',
                    'lineheight', '|', 'indent', '|',
                    'simpleupload', 'imagenone', 'imageleft', 'imageright', 'imagecenter', 'insertimage', '|',

                ],
            ],
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
