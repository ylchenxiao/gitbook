<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class UAuthForm extends Model {
	/**
	 * UAuth 登录成功后返回的附加信息
	 * @var string
	 */
	public $uc;
	/**
	 * 用于登录二次验证
	 * @var string
	 */
	public $token;
	/**
	 * 自动登录有效期
	 * @var boolean
	 */
	public $rememberMe = true;
	/**
	 * 用户验证类
	 * @var mixed
	 */
	private $_user = false;
	/**
	 * @return array the validation rules.
	 */
	public function rules() {
		return [
			// username and password are both required
			[['uc'], 'required'],
			['uc', 'validateToken'],
		];
	}

	/**
	 * 登录判断
	 */
	public function login() {
		if ($this->validate()) {
			if ($this->getUser()) {
				return Yii::$app->user->login($this->_user, $this->rememberMe ? 3600 * 24 * 30 : 0);
			}
		}
		UAuth::gotoLogin();
	}

	/**
	 * 通过 hash 获取用户信息
	 */
	public function getUser() {
		if ($this->_user === false) {
			$this->_user = UAuth::getUserInfoByToken($this->token);

		}
		return $this->_user;
	}

	public function validateToken($var_name) {
		$this->token = UAuth::validateToken($this->$var_name);
		return (bool) $this->token;
	}

	public function logout($backurl = '') {
		UAuth::logout($backurl);
		return true;
	}

}
