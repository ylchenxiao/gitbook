<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "menu".
 *
 * @property int $id 菜单编号
 * @property string $name 菜单名
 * @property string $label 显示名
 * @property string $desc 描述
 * @property string $class css
 * @property string $icon 图标
 * @property string $url 连接
 * @property int $pid 父菜单
 * @property int $level 菜单层级
 * @property int $type 类型{1:系统菜单;2:项目文档}
 * @property int $access_type 访问控制{1:游客;2:非游客}
 * @property int $sort 排序字段
 * @property string $ctime 创建时间
 * @property string $mtime 修改事件
 * @property string $extend 扩展字段
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url', 'access_type'], 'required'],
            [['pid', 'level', 'type', 'access_type', 'sort'], 'integer'],
            [['ctime', 'mtime'], 'safe'],
            [['name', 'label', 'class', 'icon', 'url'], 'string', 'max' => 255],
            [['desc'], 'string', 'max' => 1023],
            [['extend'], 'string', 'max' => 511],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '菜单编号',
            'name' => '菜单名',
            'label' => '显示名',
            'desc' => '描述',
            'class' => 'css',
            'icon' => '图标',
            'url' => '连接',
            'pid' => '父菜单',
            'level' => '菜单层级',
            'type' => '类型',
            'access_type' => '访问控制',
            'sort' => '排序字段',
            'ctime' => '创建时间',
            'mtime' => '修改事件',
            'extend' => '扩展字段',
        ];
    }

    /**
     * 自动更新字段
     */
    public function behaviors() {
        return [ [
            'class' => TimestampBehavior::className(),
            'createdAtAttribute' => 'ctime',// 自己根据数据库字段修改
            'updatedAtAttribute' => 'mtime', // 自己根据数据库字段修改, // 自己根据数据库字段修改
            //'value'   => new Expression('NOW()'),
            'value'   => function(){return date('Y-m-d H:i:s',time());},
        ], [
            'class' => BlameableBehavior::className(),
            'createdByAttribute' => 'level',// 自己根据数据库字段修改
            'updatedByAttribute' => 'level', // 自己根据数据库字段修改, // 自己根据数据库字段修改
            //'value'   => new Expression('NOW()'),
            'value'   => function(){
                if(empty($this['pid'])){return 1;}
                $parent = self::findOne($this['pid']);
                if(empty($parent['level'])){return 1;}
                return $parent['level']+1;
            },
        ],];
    }

    public static function SelectPid(){
        $menus = self::find()
            // ->cache(1800)
            ->indexBy('id')
            ->asArray()
            ->all();
        $select = [];
        foreach ($menus as $id => $item) {
            $select[$id] = $item['label'];
        }
        return $select;
    }
}
