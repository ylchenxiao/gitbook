<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MachineInfo;

/**
 * MachineInfoSearch represents the model behind the search form of `app\models\MachineInfo`.
 */
class MachineInfoSearch extends MachineInfo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ctime', 'mtime'], 'integer'],
            [['project', 'machine_room', 'domain', 'machine_type', 'ip', 'machine_remarks', 'business_desc', 'remarks', 'extend', 'author', 'modifer'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MachineInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ctime' => $this->ctime,
            'mtime' => $this->mtime,
        ]);

        $query->andFilterWhere(['like', 'project', $this->project])
            ->andFilterWhere(['like', 'machine_room', $this->machine_room])
            ->andFilterWhere(['like', 'domain', $this->domain])
            ->andFilterWhere(['like', 'machine_type', $this->machine_type])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'machine_remarks', $this->machine_remarks])
            ->andFilterWhere(['like', 'business_desc', $this->business_desc])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'extend', $this->extend])
            ->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'modifer', $this->modifer]);

        return $dataProvider;
    }
}
