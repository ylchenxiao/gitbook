<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "machine_info".
 *
 * @property int $id 主键
 * @property string $project 项目
 * @property string $machine_room 机房信息
 * @property string $domain 域名信息
 * @property string $machine_type 机器类型
 * @property string $ip ip
 * @property string $machine_remarks 机器备注
 * @property string $business_desc 业务描述
 * @property string $remarks 备注
 * @property string $extend 扩展
 * @property int $ctime 创建时间
 * @property int $mtime 修改时间
 * @property string $author 创建者
 * @property string $modifer 修改者
 */
class MachineInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'machine_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ctime', 'mtime'], 'integer'],
            [['project', 'machine_room', 'domain', 'author', 'modifer'], 'string', 'max' => 255],
            [['machine_type', 'ip', 'machine_remarks', 'business_desc'], 'string', 'max' => 127],
            [['remarks', 'extend'], 'string', 'max' => 1047],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键',
            'project' => '项目',
            'machine_room' => '机房信息',
            'domain' => '域名信息',
            'machine_type' => '机器类型',
            'ip' => 'ip',
            'machine_remarks' => '机器备注',
            'business_desc' => '业务描述',
            'remarks' => '备注',
            'extend' => '扩展',
            'ctime' => '创建时间',
            'mtime' => '修改时间',
            'author' => '创建者',
            'modifer' => '修改者',
        ];
    }

    public static function uniqueColumn($filed)
    {
        return array_keys(MachineInfo::find()
            ->asArray()
            ->indexBy($filed)
            ->all());
    }

    public function tree()
    {
        $machines = MachineInfo::find()
            ->asArray()
            ->orderBy('project, machine_room, machine_type')
            //178+小程序
            // ->where('project in ("178+小程序")')
            ->indexBy('id')
            ->all();
        $machinesFmt = [];
        $project = array_unique(array_column($machines, 'project'));
        foreach ($machines as $machine) {
            $machinesFmt[$machine['project']][$machine['machine_room']][$machine['machine_type']][]
                = $machine['id'];
        }
        $tree = [];
        foreach ($machinesFmt as $project => $machineRooms) {
            $machinesFmt[$project] = [
                'name' => $project,
                'description' => $project,
                'href' => '#',
            ];
            foreach ($machineRooms as $machineRoom => $machineTypes) {
                if ($machineRoom == 'SVN地址') {
                    continue;
                }
                $machinesFmt[$project]['children'][$machineRoom] = [
                    'name' => $machineRoom,
                    'description' => $machineRoom,
                    'href' => '#',
                ];
                foreach ($machineTypes as $machineType => $machines2) {
                    $machinesFmt[$project]['children'][$machineRoom]['children'][$machineType] = [
                        'name' => $machineType,
                        'description' => $machineType,
                        'href' => '#',
                    ];
                    foreach ($machines2 as $machineId) {
                        $machine = $machines[$machineId];
                        $machinesFmt[$project]['children'][$machineRoom]['children'][$machineType]['children'][] = [
                            'name' => "{$machine['ip']}({$machine['machine_remarks']})",
                            'description' => $machine['remarks'] . $machine['machine_remarks'],
                            'href' => '/machine-info/view?id=' . $machineId,
                        ];

                    }

                }
            }
        }
        return $machinesFmt;
    }
}
