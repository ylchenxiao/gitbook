<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%demo_table}}".
 *
 * @property int $id  
 */
class DemoTable extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%demo_table}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => ' ',
        ];
    }
}
