<?php

namespace app\models;

use Yii;

/**
 * 业务模型类的基类
 * 自动填充时间属性和用户属性
 */
class BaseModel extends \yii\db\ActiveRecord
{
    protected $_auto_fill_time = true;
    protected $_auto_fill_time_behavior = 'time_attr';
    protected $_create_time_attr = 'ctime';
    protected $_update_time_attr = 'mtime';

    protected $_auto_fill_user = true;
    protected $_auto_fill_user_behavior = 'author_attr';
    protected $_create_user_attr = 'author';
    protected $_update_user_attr = 'modifer';

    public function autoFillTimeValue($event)
    {
        return time();
    }
    public function autoFillUserValue($event)
    {
        return Yii::$app->user->identity->username;
    }
    public function behaviors()
    {
        $behaviors = [];
        if ($this->_auto_fill_time) {
            $behaviors[$this->_auto_fill_time_behavior] = [
                'class'              => \yii\behaviors\TimestampBehavior::class,
                'createdAtAttribute' =>  'ctime',
                'updatedAtAttribute' => 'mtime',
                'value'              =>[$this, 'autoFillTimeValue'],
            ];
        }
        if ($this->_auto_fill_user) {
            $behaviors[$this->_auto_fill_user_behavior] = [
                'class' => \yii\behaviors\BlameableBehavior::class,
                'createdByAttribute' => 'author',
                'updatedByAttribute' => 'modifer',
                'value'              =>[$this, 'autoFillUserValue'],
            ];
        }
        return $behaviors;
    }
}
