<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "issue_log".
 *
 * @property int $id 主键
 * @property string $cate 问题分类
 * @property string $title 问题概要
 * @property string $reason 问题原因
 * @property string $issue 问题详情
 * @property string $fix 解决过程
 * @property string $extend 扩展
 * @property int $ctime 创建时间
 * @property int $mtime 修改时间
 * @property string $author 创建者
 * @property string $modifer 修改者
 */
class IssueLog extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'issue_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['issue', 'fix'], 'string'],
            [['ctime', 'mtime', 'week'], 'integer'],
            [['cate', 'title', 'author', 'modifer'], 'string', 'max' => 255],
            [['reason', 'extend'], 'string', 'max' => 1047],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键',
            'cate' => '问题分类',
            'title' => '问题概要',
            'reason' => '问题原因',
            'issue' => '影响',
            'fix' => '解决过程',
            'extend' => '扩展',
            'week' => '第几周',
            'ctime' => '创建时间',
            'mtime' => '修改时间',
            'author' => '创建者',
            'modifer' => '修改者',
        ];
    }
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['week'] = [
            'class' => \yii\behaviors\AttributeBehavior::class,
            'attributes' => [
                \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'week',
            ],
            'value' => function ($event) {
                return date('W');
            },
        ];
        return $behaviors;
    }
}
