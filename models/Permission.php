<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\models\Menu;

/**
 * This is the model class for table "permission".
 *
 * @property int $id 主键id
 * @property string $user_id 用户名
 * @property string $user_name 用户昵称
 * @property int $menu_id 菜单编号
 * @property string $menu_name 菜单名称
 * @property string $mtime 修改时间
 * @property string $ctime 创建时间
 * @property string $extend 备用字段
 */
class Permission extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'permission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id'], 'integer'],
            [['mtime', 'ctime'], 'safe'],
            [['user_id'], 'string', 'max' => 24],
            [['user_name', 'menu_name'], 'string', 'max' => 255],
            [['extend'], 'string', 'max' => 511],
            [['user_id', 'menu_id'], 'unique', 'targetAttribute' => ['user_id', 'menu_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键id',
            'user_id' => '用户名',
            'user_name' => '用户昵称',
            'menu_id' => '菜单编号',
            'menu_name' => '菜单名称',
            'mtime' => '修改时间',
            'ctime' => '创建时间',
            'extend' => '备用字段',
        ];
    }
    /**
     * 自动更新字段
     */
    public function behaviors() {
        return [ [
            'class' => TimestampBehavior::className(),
            'createdAtAttribute' => 'ctime',// 自己根据数据库字段修改
            'updatedAtAttribute' => 'mtime', // 自己根据数据库字段修改, // 自己根据数据库字段修改
            //'value'   => new Expression('NOW()'),
            'value'   => function(){return date('Y-m-d H:i:s',time());},
        ], [
            'class' => BlameableBehavior::className(),
            'createdByAttribute' => 'menu_name',// 自己根据数据库字段修改
            'updatedByAttribute' => 'menu_name', // 自己根据数据库字段修改, // 自己根据数据库字段修改
            //'value'   => new Expression('NOW()'),
            'value'   => function(){
                if(empty($this['menu_id'])){return '';}
                $menu = Menu::findOne($this['menu_id']);
                if(empty($menu['label'])){return '';}
                return $menu['label'];
            },
        ], [
            'class' => BlameableBehavior::className(),
            'createdByAttribute' => 'user_name',// 自己根据数据库字段修改
            'updatedByAttribute' => 'user_name', // 自己根据数据库字段修改, // 自己根据数据库字段修改
            //'value'   => new Expression('NOW()'),
            'value'   => function(){
                if(empty($this['user_name'])){return $this['user_id'];}
                return $this['user_name'];
            },
        ],];
    }

    public static function Tree($user_id = '')
    {

        $permissionIds = empty($user_id) ? [] : self::find()
            ->select('menu_id')
            ->where(['user_id' => $user_id])
            ->column();
            // ->asArray()
            // ->all();
        $menus = Menu::find()
            ->select('label,icon,class,url,pid,id,type as flag')
            ->where(['in', 'id', $permissionIds])
            ->orWhere(['access_type'=>'1'])
        // ->cache(Menu::INFO_CACHE_EXPIRE)
            ->orderBy(['level' => SORT_ASC, 'sort' => SORT_DESC])
            ->asArray()
            ->all();

        $items = [];
        foreach ($menus as $menu) {
            $items[$menu['id']] = $menu;
        }
        //第二部 遍历数据 生成树状结构
        $tree = [];
        foreach ($items as $key => $item) {
            if (isset($items[$item['pid']])) {
                $items[$item['pid']]['son'][] = &$items[$key];
            } else {
                $tree[] = &$items[$key];
            }
        }
        return $tree;
    }

    public static function SelectPid(){
        $menus = self::find()
            // ->cache(1800)
            ->indexBy('id')
            ->asArray()
            ->all();
        $select = [];
        foreach ($menus as $id => $item) {
            $select[$id] = Yii::t('menu', $item['name']);
        }
        return $select;
    }
}
