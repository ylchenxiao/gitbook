<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IssueLog;

/**
 * IssueLogSearch represents the model behind the search form of `app\models\IssueLog`.
 */
class IssueLogSearch extends IssueLog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ctime', 'mtime'], 'integer'],
            [['cate', 'title', 'reason', 'issue', 'fix', 'extend', 'author', 'modifer', 'week'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IssueLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ctime' => $this->ctime,
            'mtime' => $this->mtime,
        ]);
        $query->andFilterWhere(['like', 'cate', $this->cate])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'reason', $this->reason])
            ->andFilterWhere(['like', 'issue', $this->issue])
            ->andFilterWhere(['like', 'fix', $this->fix])
            ->andFilterWhere(['like', 'extend', $this->extend])
            ->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'modifer', $this->modifer])
            ->andFilterWhere(['=', 'week', $this->week]);

        return $dataProvider;
    }
}
