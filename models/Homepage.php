<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "homepage".
 *
 * @property int $id 主键
 * @property string $title 标题
 * @property string $desc 描述
 * @property string $url 链接
 * @property string $url_name 链接描述
 * @property int $type 类型{"1":"重点","2":"普通"}
 * @property int $sort 排序（0~255)
 * @property string $ctime &创建时间
 * @property string $mtime &修改时间
 * @property string $extend &备用字段
 */
class Homepage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'homepage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'sort'], 'integer'],
            [['ctime', 'mtime'], 'safe'],
            [['title', 'url_name', 'extend'], 'string', 'max' => 255],
            [['desc', 'url'], 'string', 'max' => 1023],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键',
            'title' => '标题',
            'desc' => '描述',
            'url' => '链接',
            'url_name' => '链接描述',
            'type' => '类型',
            'sort' => '排序（0~255)',
            'ctime' => '创建时间',
            'mtime' => '修改时间',
            'extend' => '备用字段',
        ];
    }
    /**
     * 自动更新字段
     */
    public function behaviors() {
        return [ [
            'class' => TimestampBehavior::className(),
            'createdAtAttribute' => 'ctime',// 自己根据数据库字段修改
            'updatedAtAttribute' => 'mtime', // 自己根据数据库字段修改, // 自己根据数据库字段修改
            //'value'   => new Expression('NOW()'),
            'value'   => function(){return date('Y-m-d H:i:s',time());},
        ],];
    }
}
