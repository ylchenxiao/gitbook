<?php

namespace app\models;
use app\models\Permission;
use Yii;

class UAuth extends \yii\base\BaseObject implements \yii\web\IdentityInterface {
	public $id;
	public $username;
	public $hash;
	public $dn;
	public $avatar;

	private static $config = [
		'appid' => '43',
		'appkey' => '4b55f534e2a15108ad63a5d892f96d8e',
	];

	const LOGIN_URL = 'http://login.uuzu.com/access/index';
	const LOGOUT_URL = 'http://login.uuzu.com/access/logout';
	const USER_DATA_URL = 'http://login.uuzu.com/access/getData';
	const USERINFO_SESSION_KEY = __CLASS__ . '_userinfo';
	const PERMISSION_SESSION_KEY = __CLASS__ . '_permission';

	/**
	 * 登录二次验证
	 * @param  str $token 用于 UAuth 二次校验
	 * @return [type]        [description]
	 */
	public static function getUserInfoByToken($token) {
		// 参数获取
		$appid = self::$config['appid'];
		$appkey = self::$config['appkey'];

		// 获取用户信息
		$url = self::USER_DATA_URL . "?appid={$appid}&uc={$token}";
		$result = json_decode(file_get_contents($url), true);
		// 用户信息获取失败1
		if (empty($result['result']) or $result['result'] !== 1 or empty($result['data'])) {
			return null;
		}
		$data = $result['data'];
		if (empty($data['username'])) {
			return null;
		}
		$userInfo = [
			'id' => $data['username'],
			'username' => $data['displayName'] ?? '',
			'hash' => $data['hash'] ?? '',
			'dn' => $data['dn'] ?? '',
			'avatar' => self::_getUserAvatar($data['username']),
		];
		Yii::$app->session[self::USERINFO_SESSION_KEY] = $userInfo;
		return new static($userInfo);
	}
	/**
	 * uc 信息校验
	 * @param  	str 	$uc 	[登录成功后返回的附加信息]
	 * @return 	bool
	 */
	public static function validateToken($uc) {
		$appid = self::$config['appid'];
		$appkey = self::$config['appkey'];

		//登录信息解密
		$ucdata = base64_decode($uc);
		$data = json_decode(substr($ucdata, 32), true);
		$data_hash = substr($ucdata, 0, 32);
		if (empty($data['time']) or empty($data['PHPSESSID'])) {
			return false;
		}
		if (md5($appkey . $data['time'] . $data['PHPSESSID']) !== $data_hash) {
			return false;
		}
		return md5($appkey . $data_hash);
	}
	/**
	 * 获取用户权限
	 * @return array     权限数组
	 */
	public function getMenuList() {
		$menus = Permission::getMenuListTree($this->getId());
		return $menus;
	}
	/**
	 * 跳转到登录页面
	 */
	public static function gotoLogin($info = 'error', $code = -1) {
		$backurl = self::$config['backurl'] = Yii::$app->request->getHostInfo() . Yii::$app->request->url;
		$appid = self::$config['appid'];
		$url = self::LOGIN_URL . '?appid=' . $appid . '&backurl=' . urlencode(base64_encode($backurl));
		self::_redirect($url);
	}
	/**
	 * 重定向
	 */
	private static function _redirect($url) {
		header("Location: " . $url);
	}

	public static function logout($backurl) {
		empty($backurl) and $backurl = Yii::$app->request->getHostInfo();
		Yii::$app->session[self::USERINFO_SESSION_KEY] = '';
		header("Location: " . self::LOGOUT_URL . '?backurl=' . $backurl);
	}
	public static function findIdentity($id) {
		$userInfo = Yii::$app->session->get(self::USERINFO_SESSION_KEY, null);
		if (!empty($userInfo['id']) and $id == $userInfo['id']) {
			return new static($userInfo);
		}
		return null;
	}

	public static function findIdentityByAccessToken($token, $type = null) {
		return null;
	}

	public function getId() {
		return $this->id;
	}

	public function getAuthKey() {
		return '';
	}

	public function validateAuthKey($authKey) {
		return true;
	}

	private static function _getUserAvatar($username) {
		$avatar = 'http://community.youzu.com/uc_server/avatar.php?uid=3306&size=middle';
		if (!empty($username)) {
			$avatarApiResult = file_get_contents("http://community.youzu.com/api.php?mod=avatar&account={$username}&type=single");
			$avatarApiResult = json_decode($avatarApiResult, true);
			if (isset($avatarApiResult['data']['url'])) {
				if ($avatarApiResult['status'] == 0) {
					$avatar = $avatarApiResult['data']['url'];
					return $avatar;
				}
			}
		}
		return $avatar;
	}
}
